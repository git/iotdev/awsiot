Thanks for being an early adopter, but the party has moved!

This repository is now obsolete.  Development has migrated to this new
repository, based on Amazon's official AWS IoT repo:

      https://git.ti.com/iotdev/aws-iot-device-sdk-embedded-c

We hope to see you there!

For more information, see:

      http://processors.wiki.ti.com/index.php/AWS_IoT_Development
